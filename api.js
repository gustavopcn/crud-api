// importando o HTTP para construir o server (lib padrão do JS para servir dados 
// sob protoloco HTTP)
var http = require('http');

const fs = require('fs');
// const callback = (err) => {
//     if (err) throw err;
//     console.log('The file has been saved!');
// }

// criando servidor a partir do http importado na linha acima.
var server = http.createServer(function (request, response) {

    // O createServer recebe como parâmetro uma função para gerenciar dois objetos: request e response
    response.writeHead(200, { "Content-Type": "text/html" });

    // capturando a url que foi chamada e tratando a resposta
    if (request.url == "/") {

        response.write("<h1>Pagina principal</h1>");
    }
    // capturando a url que foi chamada e tratando a resposta
    else if (request.url == "/bemvindo") {

        response.write("<h1>Bem-vindo :)</h1>");

        // fs.writeFile('message.txt', 'Você entrou na API do Gustavo!', 'utf8', callback);

        // response.send({"message" : "ok"})
    }
    
    // capturando a url que foi chamada e tratando a resposta
    else {

        response.write("<h1>Página não encontrada :(</h1>");

    }

    // Finalizando a resposta para fechar a conexão e entregar
    response.end();
});

// configuração da porta que o servidor escuta
server.listen(3000, function () {
    console.log('Servidor rodando!');
});