/**
 * Usando a biblioteca do EXPRESS para que tenha uma responsta em JSON.
 */

var express = require('express'); // Importa a lib express

var app = express();// Inicia a lib

app.get('/', function(req, res) {

    res.send("<h1>Pagina principal</h1>")  

});

app.get('/bemvindo', function(req, res){

    res.send({"message": "bem-vindo!"});
});

app.use(function(req, res, next) {
    res.send('<h2>Pagina não encontrada!</h2>');
});


app.listen(3000, function () {
    console.log('Servidor rodando!');
});